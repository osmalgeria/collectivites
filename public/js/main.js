//const momoent = new moment()
//import moment from "moment-with-locales.js"
  

//moment.locale('fr');
const spinner = document.getElementById("spinner");
const communes_div = document.getElementById("result");
const adm = document.getElementById("adm");

const communes_table = document.createElement("table");
communes_table.setAttribute("id", "table_result");
// communes_table.setAttribute("data-toggle", "table");
// communes_table.setAttribute("data-pagination", "true");
// communes_table.setAttribute("data-search", "true");

const pre = document.createElement("pre");
const table_result = document.createElement("table_result");
let h1tag = document.createElement("h2");
let fields = [
  "Ordre",
  "Nom",
  "français",
  "arabe",
  "kabyle",
  "tifinagh",
  "référence",
  "population",
  "Utilisateur",
  "Dernière édition",
  "Info / Edition"
];
//communes_table.classList.add("table", "table-bordered", "table-striped", "table-responsive");
String.prototype.sansAccent = function(){
  var accent = [
      /[\300-\306]/g, /[\340-\346]/g, // A, a
      /[\310-\313]/g, /[\350-\353]/g, // E, e
      /[\314-\317]/g, /[\354-\357]/g, // I, i
      /[\322-\330]/g, /[\362-\370]/g, // O, o
      /[\331-\334]/g, /[\371-\374]/g, // U, u
      /[\321]/g, /[\361]/g, // N, n
      /[\307]/g, /[\347]/g, // C, c
  ];
  var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
   
  var str = this;
  for(var i = 0; i < accent.length; i++){
      str = str.replace(accent[i], noaccent[i]);
  }
   
  return str;
}
let myInit = {
  method: "GET",
  headers: {
    "Content-Type": "application/json"
  },
  mode: "cors",
  cache: "force-cache"
};
let myRequest = new Request("./wilayas.json", myInit);

fetch(myRequest)
  .then(response => {
    return response.json();
  })
  .then(data => {
    let wilayas = data.wilayas;
    wilayas.forEach(wilaya => {
      let option = document.createElement("option");
      option.value = wilaya["rel_area"];
      option.innerHTML = wilaya["ref"] + ": " + wilaya["name_fr"];
      selector.appendChild(option);
      
    });
    $('#selector').select2();
    // selector.select2();
  })
  .catch(function(error) {
    console.error("Something went wrong! ", error);
  });

let url = "https://overpass-api.de/api/interpreter?data=";

function fetchData() {

  let area = parseInt(selector[selector.selectedIndex].value);

  area_name = selector[selector.selectedIndex].text.replace(/([0-9]+:\s+)/g, '').replace(/\s+/g, '_').sansAccent().toLowerCase();
  // if (area_name == "m'sila") area_name = "msila"
  // if (area_name == "el_m'ghair") area_name = "el_mghair"
  area_name = (area_name == "m'sila" || area_name == "el_m'ghair") ? area_name.replace(/'/g, "") : area_name
  if ( area == 0) {return}
  spinner.className = "show";
  console.time();
  var t0 = performance.now();
  fetch("./wilayas.json")
    .then(response => {
      return response.json();
    })
    .then(data => {
      
      let admin_level = document.getElementById("adm").value;
     
      let query = /*html*/`[out:json][timeout:300];
area(${area})->.${area_name};
(
  relation
    ["admin_level"="${admin_level}"](area.${area_name});
);
out meta center;
>;
out count;`;
      pre.innerHTML = `<code>${query}</code>`;
      hljs.highlightBlock(pre);
      queryModale.appendChild(pre);
      full_url = url + encodeURI(query);

      fetch(full_url)
        .then(response => {
          return response.json();
        })
        .then(function(obj) {
          let test = obj.elements.slice(0, -1);
          showTable(obj.elements.slice(0, -1));
          var t1 = performance.now();
          console.log("Overpass query took " + ((Math.ceil(t1 - t0))/1000).toFixed(2) + " seconds.");
          console.timeEnd(); 
        })
        .catch(function(error) {
          spinner.className = spinner.className.replace("show", "");
          console.error("Something went wrong! ", error);
        });
    });
}

// $(document).ready(function() {
//     selector.select2();
// });

wilayaDe = name => {
  const voyelles = ["a", "e", "i", "o", "u"];
  const firstLetter = name[0].toLowerCase();
  if (voyelles.includes(firstLetter) && name != "Ouargla" && name != "Annaba") return `de la wilaya d'<strong>${name}</strong>`;
  else return /*html*/`de la wilaya de <strong>${name}</strong>`;
}

function showTable(data) {
  let table_html = "";
  let i = 1;
  selDiv = adm[adm.selectedIndex].innerHTML;
  selWilaya = selector[selector.selectedIndex].innerHTML.substr(4);
  entitiesCount = data.length;
  let h1title = `Liste des <strong>${entitiesCount}</strong> ${selDiv} ${wilayaDe(selWilaya)}`;
  h1tag.innerHTML = h1title;
  table_html = "<thead><tr>";
  fields.forEach(field => {
    if ( selDiv === "Daïras" && (field === "ref" || field === "population") ) {
      return
    }
    table_html +=  `<th>${field}</th>`;
  });
  table_html += "</tr></thead>";
  console.log(data);
  let wilaya_pop = 0;
  data.forEach(element => {
    table_html += /*html*/` 
    <tr>
      <td>${i}</td>
      <td>${element.tags.name}</td>
      <td>${element.tags["name:fr"]}</td>
      <td>${element.tags["name:ar"]}</td>
      <td>${element.tags["name:kab"]}</td>
      <td>${element.tags["name:ber"]}</td>`
    if (adm[adm.selectedIndex].value != "6") {
      wilaya_pop += parseInt( element.tags["population"]);
        table_html += /*html*/`<td>${element.tags["ref"] ? element.tags["ref"] : '----'}</td>
        <td>${element.tags["population"] ? element.tags["population"] : '----'}</td>`
      }
      
      table_html += `
      <td>${user_link(element.user)}</td>
      <td title="${lastEdit(element.timestamp)}">${elapsedTime(element.timestamp)}</td>
      <td>${editLink(element.id, element.tags["wikipedia"])}</td>
    </tr>`;
    i++;
  });
 console.log("population total: " + wilaya_pop);
  communes_table.innerHTML = table_html;
 
  communes_div.appendChild(h1tag);
  spinner.className = spinner.className.replace("show", "");
  //spinner.className = spinner.classList.remove("show");
  // console.log(communes_table);
  $('#exampleModalCenter').modal('hide');
  // $('#table_resul').DataTable();
  communes_div.appendChild(communes_table);
   $("#table_result").bootstrapTable({
    pagination: true,
    search: true,
    locale: 'fr-FR',
  })
  // table_result.DataTable( {
  //   paginate: false,
  //   scrollY: 300
  // } );
  
  
}

function user_link(user_name) {
  return /*html*/`<a href="https://www.openstreetmap.org/user/${encodeURI(user_name)}"  target="blank">${user_name}</a>`;
}

function relation_link(id, counter) {
  if (counter == undefined){
    counter = id
  }
  return /*html*/`<a href="https://www.openstreetmap.org/relation/${id}"  target="blank">${counter}</a>`;
}

function wikilink(wikipedia) {
  if (wikipedia == undefined) {
    return "";
  }
  let lang = wikipedia.substr(0, 2);
  let raw = wikipedia.substr(3);
  //https://fr.wikipedia.org/wiki/Ouled_Djellal
  return /*html*/`<a href="https://${lang}.wikipedia.org/wiki/${encodeURI(raw)}" target="blank" title="Wikipedia"><button class="icon"><img class="icon" src="images/Wikipedia-globe-icon.png"></button></a>`;
  // <a href="http://osmrm.openstreetmap.de/relation.jsp?id=${id}" target="_blank" title="Relation Manager"><button class="icon">Ma</button></a>
}

function lastEdit(timestamp){
  editDate = new Date(timestamp)
  return editDate.toLocaleDateString()
}

function elapsedTime (t){
  return moment(t).locale('fr').fromNow();
}

function editLink (id, wikipedia){
  return /*html*/`<a href="https://www.openstreetmap.org/relation/${id}" target="_blank" title="OpenStreetMap"> <button class="icon"><img class="icon" src="images/osm-logo.png"></button></a>&nbsp;
<a href="http://127.0.0.1:8111/load_object?new_layer=false&amp;relation_members=true&amp;objects=relation${id}" target="hiddenIframe" title="JOSM editor"><button class="icon"><img class="icon" src="images/josm-logo.png"></button></a>&nbsp;
<a href="https://www.openstreetmap.org/edit?editor=id&amp;relation=${id}" target="_blank" title="ID editor"><button class="icon"><img class="icon" src="images/id-logo.png"></button></a>&nbsp;
<a href="http://level0.osmz.ru/?url=relation/${id}" target="_blank" title="Level0 editor"><button class="icon">L0</button></a>&nbsp;
<a href="http://ra.osmsurround.org/analyzeRelation?relationId=${id}" target="_blank" title="Relation Analyzer"><button class="icon">An</button></a>&nbsp;
<a href="http://osmrm.openstreetmap.de/relation.jsp?id=${id}" target="_blank" title="Relation Manager"><button class="icon">Ma</button></a>&nbsp;
${wikilink(wikipedia)}`;

}
function browserLocale () {
  var lang

  if (navigator.languages && navigator.languages.length) {
    // latest versions of Chrome and Firefox set this correctly
    lang = navigator.languages[0]
  } else if (navigator.userLanguage) {
    // IE only
    lang = navigator.userLanguage
  } else {
    // latest versions of Chrome, Firefox, and Safari set this correctly
    lang = navigator.language
  }

  return lang
}
console.log('browser Locale : ' + browserLocale())

// https://en.wikipedia.org/w/api.php?action=query&titles=COVID-19_pandemic_in_Algeria&prop=langlinks&lllimit=500&format=json